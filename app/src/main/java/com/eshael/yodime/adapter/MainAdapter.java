package com.eshael.yodime.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.eshael.yodime.R;
import com.eshael.yodime.model.Response;

import java.util.List;

/**
 * Created by raphael on 11/1/16.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private Context context;
    private List<Response> responseList;
    private ColorGenerator generator = ColorGenerator.MATERIAL;
    private int rowLayout;

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView thumbnail;
        TextView email;
        TextView status;
        TextView password;

        public ViewHolder(View v)
        {
            super(v);
            thumbnail = (ImageView)v.findViewById(R.id.category_letter);
            name  = (TextView)v.findViewById(R.id.username);
            email = (TextView)v.findViewById(R.id.email);
            status = (TextView)v.findViewById(R.id.status);
            password = (TextView)v.findViewById(R.id.password);
        }
    }

    public MainAdapter(List<Response> responseList, int rowLayout, Context context) {
        this.responseList = responseList;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.name.setText("Username : "+responseList.get(position).getUsername());
        String letter = String.valueOf(responseList.get(position).getEmail().charAt(0));
        TextDrawable drawable = TextDrawable.builder().buildRect(letter.toUpperCase(), generator.getRandomColor());
        holder.thumbnail.setImageDrawable(drawable);
        holder.email.setText("Email : " +responseList.get(position).getEmail());
        holder.status.setText("Status : " +responseList.get(position).getResponse_status());
        holder.password.setText("Password : " +responseList.get(position).getPassword());
    }

    @Override
    public int getItemCount() {
        return responseList.size();
    }
}

