package com.eshael.yodime.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

/**
 * Created by raphael on 11/1/16.
 */

public class Response extends SugarRecord{

    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("response_status")
    private String response_status;
    @SerializedName("password")
    private String password;

    public Response() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResponse_status() {
        return response_status;
    }

    public void setResponse_status(String response_status) {
        this.response_status = response_status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
